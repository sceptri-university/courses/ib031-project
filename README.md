# IB031 Projekt

Projekt od IB031 Úvod do strojového učení na FI MUNI, jarní semestr 2024. Zabývá se analýzou a modelováním Australského počasí.

## Spolupráce

Pro naši spolupráci by bylo ideální, abyste své změny dělali do (vhodných) větví a pak založili merge request:

1. sebe označte jako "Asignee"
2. Mě (@sceptri) označte jako "Reviewer"

> Pokud by s tímto workflow byl problém, ozvěte se

Před sloučením větví budeme potřebovat porovnat Jupyter notebooky, což git moc neumí... Proto navrhuji, abychom:

1. před mergem z jupyter notebooků vytvořili .qmd Quarto markdown soubory (vytvoří/přepíše `EDA.qmd`). Tohoto lze docílit přes `quarto convert EDA.ipynb`
2. nechali git tyto soubory sloučit (up-to-date `EDA.qmd`)
3. přegenerovali `EDA.ipynb` podle sloučeného `EDA.qmd`. Zde stačí vyrenderovat `EDA.qmd` s `keep-ipynb: true` v hlavičce

Pokud to uděláte sami, bude to super. Pokud ne, jen mi to napište do merge requestu a já to při review udělám. 

Nicméně platí, že **vycházíme** z co nejnovějšího *main*u (větve).