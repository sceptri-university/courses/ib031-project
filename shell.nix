let
  pkgs = import <nixpkgs> {};
  unstablePkgs = import <nixos-unstable> {};
  lib = import <lib> {};
  config = import <config> {};
  configPath = builtins.getEnv "NIXOS_CONFIGURATION_DIR" + "/.";

  pythonPackages = ps: (with ps; [
    jupyterlab
    jupyter-core
    numpy
    pandas
    seaborn
    ipykernel
    ipython
    pyzmq
    virtualenv
    venvShellHook
    scipy
    openpyxl
    unstablePkgs.python311Packages.scikit-learn
    xgboost
  ]);

  rPackages = with pkgs.rPackages; [
    quarto
    rmarkdown
    languageserver
  ];

  quartoPreRelease = extraRPackages: extraPythonPackages:
    pkgs.callPackage (configPath + "/packages/quarto/preRelease.nix")
    (with pkgs lib config; {
      extraRPackages = extraRPackages;
      extraPythonPackages = extraPythonPackages;
    });
in
  pkgs.mkShell {
    venvDir = "./.venv";
    buildInputs = with pkgs;
      [
        stdenv.cc.cc.lib
        jupyter-all
        taglib
        openssl
        libxml2
        libxslt
        libzip
        zlib
        (quartoPreRelease rPackages pythonPackages)
        texlive.combined.scheme-full
        pandoc
      ]
      ++ (pythonPackages (pkgs.python311Packages));

    postVenvCreation = ''
      unset SOURCE_DATE_EPOCH
      python -m ipykernel install --user --name=myenv4 --display-name="myenv4"
    '';
    shellHook = ''
         # allow pip to install wheels
         unset SOURCE_DATE_EPOCH
      python -m ipykernel install --user --name python3-ib031 --display-name "Python (IB031)"
         echo "Welcome to IB031 Úvod do strojového učení shell"
    '';
  }
